# Библиотека, которая позволяет работать с файлами конфигураии
import configparser

# Библиотека для формирования xlsx файлов
import xlsxwriter

# Класс для создания соединения с Telegram
from telethon.sync import TelegramClient

# классы для работы с каналами телеграмма
from telethon.tl.functions.channels import GetParticipantsRequest
from telethon.tl.types import ChannelParticipantsSearch

# класс для работы с сообщениями телеграмма
from telethon.tl.functions.messages import GetHistoryRequest

# Считываем учетные данные
config = configparser.ConfigParser()
config.read("config.ini")

# Присваиваем значения внутренним переменным
api_id = config['Telegram']['api_id']
api_hash = config['Telegram']['api_hash']
username = config['Telegram']['username']

client = TelegramClient(username, api_id, api_hash)

client.start()


async def dump_all_participants(channel):
    offset_user = 0  # номер участника, с которого начинается считывание
    limit_user = 100  # максимальное число записей, передаваемых за один раз
    filter_user = ChannelParticipantsSearch('')

    workbook = xlsxwriter.Workbook('channel_participants.xlsx')
    worksheet = workbook.add_worksheet()
    i = 0

    # Запускаем бесконечный цикл
    while True:
        # С помощью компонентов библиотеки Telethon, через client, получаем список пользователей
        # Обратите внимание, что мы передаем в конструктор класса GetParticipantsRequest наш канал
        participants = await client(GetParticipantsRequest(channel,
                                                           filter_user, offset_user, limit_user, hash=0))
        # Проверяем, если у нас нет пользователей, выходим из цикла
        if not participants.users:
            break
        # Если данные есть то делаем смещение и будем в следующей итерации будем запрашивать
        # следующую порцию данных
        offset_user += len(participants.users)
        # Если пользователи есть. Начинаем обрабатывать их и записывать.
        for user in participants.users:
            # i - это текущая строка.
            # В первую ячейку мы будем записывать ID пользователя
            worksheet.write(i, 0, user.id)
            # Во вторую ячейку мы будем записывать имя пользователя
            worksheet.write(i, 1, user.first_name)
            # В третью ячейку мы будем записывать фамилию пользователя
            worksheet.write(i, 2, user.last_name)
            # ...
            worksheet.write(i, 3, user.username)
            worksheet.write(i, 4, user.phone)
            worksheet.write(i, 5, user.bot)
            # Следующего пользователя мы уже будем писать в следующую строку.
            i += 1
    # Когда все операции завершены, закрываем файл
    workbook.close()


async def dump_all_messages(channel):
    offset_msg = 0  # номер записи, с которой начинается считывание
    limit_msg = 100  # максимальное число записей, передаваемых за один раз

    workbook = xlsxwriter.Workbook('channel_messages.xlsx')
    worksheet = workbook.add_worksheet()
    i = 0

    while True:
        # Запрос истории у Telegram
        history = await client(GetHistoryRequest(
            peer=channel,
            offset_id=offset_msg,
            offset_date=None, add_offset=0,
            limit=limit_msg, max_id=0, min_id=0,
            hash=0))
        if not history.messages:
            break
        messages = history.messages

        # Смещение у сообщений считаем несоклько иначе
        offset_msg = messages[len(messages) - 1].id

        for message in messages:
            # Иногда получаем AttributeError при запросе id  пользователя, обработаем эту ошибку.
            try:
                user_id = message.from_id.user_id
            except AttributeError:
                user_id = None
            # i - это текущая строка.
            # В первую ячейку мы будем записывать ID пользователя
            worksheet.write(i, 0, user_id)
            # Во вторую ячейку мы будем записывать его сообщение
            worksheet.write(i, 1, message.message)
            # Смешаем строку на одну вниз
            i += 1
    # Закрываем файл
    workbook.close()


async def main():
    channel_ident = input("Enter the channel name or ID: ")
    channel = await client.get_entity(channel_ident)
    await dump_all_participants(channel)
    await dump_all_messages(channel)


with client:
    client.loop.run_until_complete(main())
